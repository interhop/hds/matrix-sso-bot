"""
Example Usage:

random_user
      *emoji verification or one-sided verification

random_user
      !echo something

echo_bot
      something
"""
import logging
import os

import cryptography
import simplematrixbotlib as botlib
import secrets
from keycloak import KeycloakAdmin, KeycloakPostError
from keycloak import KeycloakOpenIDConnection

config = botlib.Config()
config.encryption_enabled = True  # Automatically enabled by installing encryption support
config.emoji_verify = True
config.ignore_unverified_devices = True

matrix_server = os.environ.get("BOT_SSO_SERVER")
matrix_user = os.environ.get("BOT_SSO_USER")
matrix_password = os.environ.get("BOT_SSO_PASSWORD")

SERVER_URL=os.environ.get("BOT_SSO_KEYCLOAK_SERVER_URL")
USERNAME = os.environ.get("BOT_SSO_KEYCLOAK_USERNAME")
PASSWORD = os.environ.get("BOT_SSO_KEYCLOAK_PASSWORD")
REALM_NAME=os.environ.get("BOT_SSO_KEYCLOAK_REALM_NAME")
USER_REALM_NAME=os.environ.get("BOT_SSO_KEYCLOAK_USER_REALM_NAME")
CLIENT_ID=os.environ.get("BOT_SSO_KEYCLOAK_CLIENT_ID")
CLIENT_SECRET_KEY=os.environ.get("BOT_SSO_KEYCLOAK_SECRET_KEY")

creds = botlib.Creds(matrix_server, matrix_user, matrix_password)
bot = botlib.Bot(creds, config)
PREFIX = '!'


@bot.listener.on_message_event
async def echo(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() \
            and match.prefix() \
            and match.command("echo"):

        await bot.api.send_markdown_message(room.room_id,
                                        " ".join(arg for arg in match.args()) + " " + match.event.sender + " !!")


@bot.listener.on_message_event
async def admin(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() \
            and match.prefix() \
            and match.command("admin"):


        keycloak_connection = KeycloakOpenIDConnection(
            server_url=SERVER_URL,
            username=USERNAME,
            password=PASSWORD,
            realm_name=REALM_NAME,
            user_realm_name=USER_REALM_NAME,
            client_id=CLIENT_ID,
            client_secret_key=CLIENT_SECRET_KEY,
            verify=True)
        keycloak_admin = KeycloakAdmin(connection=keycloak_connection)
        username = match.event.sender.split(":")[0].split("@")[1]
        try:
            # Add user and set password
            import string
            alphabet = string.ascii_letters + string.digits
            secret = ''.join(secrets.choice(alphabet) for i in range(20))

            keycloak_admin.create_user({
                "username": username,
                "enabled": True,
                "credentials": [{"value": secret,"type": "password",}]
            })
            msg = f"""
            Votre username: `{username}`
            Votre mot de passe temporaire: `{secret}`
            URL pour changer votre mot de passe: `{SERVER_URL}`
            """
        except KeycloakPostError as e:
            if e.response_code == 409:
                msg = f"user: `{username}` already exist in sso"
            else:
                msg = e.error_message
        await bot.api.send_markdown_message(room.room_id, msg)


def run_bot():
    try:
        bot.run()
    except cryptography.fernet.InvalidToken:
        logging.error("The token does not seem to fit the saved session. this can happen if you change the bot user."
                      "If this is the case, deleting the session.txt and restarting the bot might help")
        exit(1)


if __name__ == "__main__":
    run_bot()
