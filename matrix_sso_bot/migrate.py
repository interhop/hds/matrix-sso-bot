import asyncio
import os

import markdown
from nio import AsyncClient, MatrixRoom, RoomMessageText, EnableEncryptionBuilder

from keycloak import KeycloakAdmin, KeycloakPostError
from keycloak import KeycloakOpenIDConnection

MATRIX_SERVER = os.environ.get("BOT_SSO_SERVER")
MATRIX_USER = os.environ.get("BOT_SSO_USER")
MATRIX_PASSWORD = os.environ.get("BOT_SSO_PASSWORD")

SERVER_URL=os.environ.get("BOT_SSO_KEYCLOAK_SERVER_URL")
USERNAME = os.environ.get("BOT_SSO_KEYCLOAK_USERNAME")
PASSWORD = os.environ.get("BOT_SSO_KEYCLOAK_PASSWORD")
REALM_NAME=os.environ.get("BOT_SSO_KEYCLOAK_REALM_NAME")
USER_REALM_NAME=os.environ.get("BOT_SSO_KEYCLOAK_USER_REALM_NAME")
CLIENT_ID=os.environ.get("BOT_SSO_KEYCLOAK_CLIENT_ID")
CLIENT_SECRET_KEY=os.environ.get("BOT_SSO_KEYCLOAK_SECRET_KEY")

async def message_callback(room: MatrixRoom, event: RoomMessageText) -> None:
    print(
        f"Message received in room {room.display_name}\n"
        f"{room.user_name(event.sender)} | {event.body}"
    )

async def create_user(username):
    import secrets
    # see https://stackoverflow.com/a/45086313 to setup the admin client
    keycloak_connection = KeycloakOpenIDConnection(
        server_url=SERVER_URL,
        username=USERNAME,
        password=PASSWORD,
        realm_name=REALM_NAME,
        user_realm_name=USER_REALM_NAME,
        client_id=CLIENT_ID,
        client_secret_key=CLIENT_SECRET_KEY,
        verify=True)
    keycloak_admin = KeycloakAdmin(connection=keycloak_connection)
    try:
        # Add user and set password
        import string
        alphabet = string.ascii_letters + string.digits
        secret = ''.join(secrets.choice(alphabet) for i in range(20))

        keycloak_admin.create_user({
            "username": username,
            "enabled": True,
            "credentials": [{"value": secret,"type": "password",}],
            "requiredActions": ["UPDATE_PASSWORD"],
        })
        msg = f"""
            Votre username: `{username}`
            Votre mot de passe temporaire: `{secret}`
            URL pour changer votre mot de passe: `{SERVER_URL}`
            """
    except KeycloakPostError as e:
        if e.response_code == 409:
            msg = f"user: `{username}` already exist in sso"
        else:
            msg = e.error_message
    return msg

async def main(users) -> None:
    client = AsyncClient(MATRIX_SERVER, MATRIX_USER)
    client.add_event_callback(message_callback, RoomMessageText)

    print(await client.login(MATRIX_PASSWORD))
    for user in users:
        result = await client.room_create(
            initial_state=[EnableEncryptionBuilder().as_dict()], is_direct=False, invite=[user])

        msg = await create_user(user.split(":")[0].split("@")[1])
        await client.room_send(
            room_id=result.room_id,
            message_type="m.room.message",
            content={
                "msgtype": "m.text",
                "body": msg,
                "format": "org.matrix.custom.html",
                "formatted_body": markdown.markdown(msg,
                                                    extensions=['fenced_code', 'nl2br'])
            }
        )
    await client.sync_forever(timeout=30000)  # milliseconds


if __name__ == '__main__':
    users = "@parisni:matrix-test.interhop.org".split(",")
    asyncio.run(main(users))
